# Neon on Docker

This repository contains instructions on getting setup in the latest Docker version in KDE Neon based on Ubuntu 20.04. This document makes use of [x11docker](https://github.com/mviereck/x11docker), a wrapper script used for running GUI applications inside docker. This script is maintained against latest Docker versions.

## Remove existing configurations, docker containers and docker

This is not necessary, unless you may have gone down some very deep rabbit holes and need a pristine environment to work with.

**Warning**: This will remove all docker configurations, docker instances and docker.

```
sudo apt purge docker docker.io containerd runc
sudo apt purge autopurge
sudo rm -rf /var/lib/docker
```

## Prerequisites
This should be a one time event per system installation.

```
# Update repositories.
sudo apt update
# Install Docker and Xephyr.
sudo apt install docker.io xserver-xephyr
# Install x11docker.
curl -fsSL https://raw.githubusercontent.com/mviereck/x11docker/master/x11docker | sudo bash -s -- --update
# Add active user to docker group.
sudo usermod -aG docker $(whoami)
# Activate new group in this terminal.
newgrp docker
# Unmask service and socket so they can be used.
sudo systemctl unmask docker.service
sudo systemctl unmask docker.socket
# Start docker service.
sudo systemctl start docker.service
```

From this point forward, any new terminals started by the user should have the docker group enabled and docker should automatically start on future reboots. Any preexisting terminals will require the use of `newgrp docker`.

## Running KDE Neon in Docker
Select a [viable tag](https://hub.docker.com/r/kdeneon/plasma/tags) to pull from docker. We will use 
`kdeneon/plasma:unstable` below.

```
# Pull the latest kdeneon/plasma:unstable image.
docker pull kdeneon/plasma:unstable
# Now start a temporary instance.
x11docker --pulseaudio --init=systemd --user=RETAIN --sudouser --clipboard --desktop --size 1024x768 -- --rm kdeneon/plasma:unstable
# Alternatively, just run a single program.
x11docker --pulseaudio --init=systemd --user=RETAIN --sudouser --clipboard --size 1024x768 -- --rm kdeneon/plasma:unstable okular
```

That should be it, you should have a working KDE Neon Plasma environment!

### What do all these parameters mean?

| Parameters | Description |
| --- | --- |
| `--pulseaudio` | Enables audio through pulse audio system. |
| `--init=systemd` | Starts container with systemd init system |
| `--user=RETAIN` | Keeps default user (neon) instead of renaming to current user. |
| `--sudouser` | Allows you to sudo inside the container. |
| `--clipboard` | Shares clipboard between host and container |
| `--desktop` | Tell x11docker we are launching a desktop rather than an application. |
| `--size 1024x768` | Set resolution to 1024x768 |
| `--` | Pass remaining options to Docker |
| `--rm` | Delete container after container closes |

### Useful parameters

- Passing  `-v ~/src:/home/neon/src` to docker is useful for mapping paths, such as in this example, this takes `~/src` from the host system and makes it available in the container under `/home/neon/src`.
- `--gpu` enables 3d acceleration.
- `--webcam` enables webcam support.

### More parameters

- [x11docker options](https://github.com/mviereck/x11docker#options)
- [Docker options](https://docs.docker.com/engine/reference/commandline/run/)
